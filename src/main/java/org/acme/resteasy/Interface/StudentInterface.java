package org.acme.resteasy.Interface;

import org.acme.resteasy.Model.Student;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URISyntaxException;
import java.util.List;

public interface StudentInterface {

    @GET
    public List<Student> getallstudents(@QueryParam("major") String major, @QueryParam("eyear") String eyear, @QueryParam("start") int start, @QueryParam("size") int size);
    @GET
    @Path("/{studentId}")
    public Student getstudentbyid(@PathParam("studentId") Long studentId);
    @POST
    public Response addstudent(Student student , @Context UriInfo uriInfo) throws URISyntaxException;
    @PUT
    @Path("/{studentId}")
    public Student updatestudent(@PathParam("studentId") int studentId,Student student);
    @DELETE
    @Path("/{studentId}")
    public String deletestudent(@PathParam("studentId") Long studentId);



}
