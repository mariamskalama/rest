package org.acme.resteasy.Interface;

import org.acme.resteasy.Model.Class;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URISyntaxException;
import java.util.List;


public interface ClassInterface {
    @GET
    public List<Class> getallclasses(@QueryParam("name") String name, @QueryParam("start") int start, @QueryParam("size") int size);
    @GET
    @Path("/{classId}")
    public Class getclassbyid(@PathParam("classId") Long classId);
    @POST
    public Response addclass(Class x , @Context UriInfo uriInfo) throws URISyntaxException;
    @PUT
    @Path("/{classId}")
    public Class updateclass(@PathParam("classId") int classId,Class x);
    @DELETE
    @Path("/{classId}")
    public String deleteclass(@PathParam("classId") Long classId);

}
