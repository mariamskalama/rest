package org.acme.resteasy.Resource;

import org.acme.resteasy.Exception.DataNotFoundException;
import org.acme.resteasy.Interface.ClassInterface;
import org.acme.resteasy.Model.Class;
import org.acme.resteasy.Service.ClassService;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/classes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)


public class ClassResource implements ClassInterface {
   ClassService c = new ClassService();

    public List<Class> getallclasses(@QueryParam("name") String name, @QueryParam("start") int start, @QueryParam("size") int size) {

        if(name!=null && name.length()>0)
            return c.getAllClassesName(name);
        if(start>=0&&size>0)
            return c.getAllClassesPaginated(start,size);
        return (c.getAllClasses());
    }



    public Class getclassbyid(@PathParam("classId") Long classId) {
        Class x= c.getClass(classId);
        if(x==null)
            throw new DataNotFoundException("Class with id "+classId+" does not exist");
        return x;
    }


    public Response addclass(Class x , @Context UriInfo uriInfo) throws URISyntaxException {
        Class newclass=c.addClass(x);
        URI uri=uriInfo.getAbsolutePathBuilder().path(String.valueOf(newclass.getId())).build();
        return Response.created(uri).entity(newclass).build();
    }



    public Class updateclass(@PathParam("classId") int classId,Class x) {
        x.setId(classId);
        return c.updateClass(x);
    }



    public String deleteclass(@PathParam("classId") Long classId){
        c.removeClass(classId);
        return "deleted";
    }
}
