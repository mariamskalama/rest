package org.acme.resteasy.Resource;
import org.acme.resteasy.Exception.DataNotFoundException;
import org.acme.resteasy.Interface.StudentInterface;
import org.acme.resteasy.Model.Student;
import org.acme.resteasy.Service.StudentService;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/students")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class StudentResource implements StudentInterface {
    StudentService s = new StudentService();

    @GET
    public List<Student> getallstudents(@QueryParam("major") String major, @QueryParam("eyear") String eyear, @QueryParam("start") int start, @QueryParam("size") int size)
    {
       if(major!=null && major.length()>0)
           return s.getAllStudentsMajor(major);
       if(eyear!=null)
            return s.getAllStudentsEyear(eyear);
        if(start>=0&&size>0)
            return s.getAllStudentsPaginated(start,size);
        return (s.getAllStudents());
    }

    @GET
    @Path("/{studentId}")
    public Student getstudentbyid(@PathParam("studentId") Long studentId)
    {
        Student student= s.getStudent(studentId);
        if(student==null)
            throw new DataNotFoundException("Student with id "+studentId+" does not exist");
        return student;
    }

    @POST
    public Response addstudent(Student student , @Context UriInfo uriInfo) throws URISyntaxException
    {
        Student newstudent=s.addStudent(student);
        URI uri=uriInfo.getAbsolutePathBuilder().path(String.valueOf(newstudent.getId())).build();
        return Response.created(uri).entity(newstudent).build();

    }


    @PUT
    @Path("/{studentId}")
    public Student updatestudent(@PathParam("studentId") int studentId,Student student)
    {
        student.setId(studentId);
        return s.updateStudent(student);
    }

    @DELETE
    @Path("/{studentId}")
    public String deletestudent(@PathParam("studentId") Long studentId)
    {
        s.removeStudent(studentId);
        return "deleted";
    }


    }