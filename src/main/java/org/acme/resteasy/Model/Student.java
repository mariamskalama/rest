package org.acme.resteasy.Model;

public class Student {
    private int id;
    private String name;
    private String eyear;
    private String major;


    public Student(){}
    public Student(int id, String name, String eyear, String major) {
        this.id = id;
        this.name = name;
        this.eyear = eyear;
        this.major = major;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEyear(String eyear) {
        this.eyear = eyear;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEyear() {
        return eyear;
    }

    public String getMajor() {
        return major;
    }
}
