package org.acme.resteasy.Service;

import org.acme.resteasy.DB.Database;
import org.acme.resteasy.Model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class StudentService {

    private Map<Long, Student> Students= Database.Students;

    public StudentService(){
        Students.put(1L,new Student(1,"Mariam","2015","MET"));
        Students.put(2L,new Student(2,"Boudi","2015","MET"));

    }


    public List<Student> getAllStudents(){
        return new ArrayList<Student>(Students.values());
}
    public Student getStudent(Long id){
        return Students.get(id);
    }

    public Student addStudent(Student Student){
        Student.setId(Students.size()+1);
        Students.put((long) Student.getId(),Student);
        return Student;
    }

    public Student updateStudent(Student Student){
        if(Student.getId()<=0)
            return null;
        else
        {
            Students.put((long) Student.getId(),Student);
            return Student;
        }
    }

    public void removeStudent(Long Student){
        Students.remove(Student);
    }

    public List<Student> getAllStudentsEyear(String eyear) {
      List<Student> studentsforyear=new ArrayList<>();
      for(Student student:Students.values()){
         if(student.getEyear().equals(eyear))
             studentsforyear.add(student);
      }
        return studentsforyear;

    }

    public List<Student> getAllStudentsMajor(String major) {
        List<Student> studentsformajor=new ArrayList<>();
        for(Student student:Students.values()){
            if(student.getMajor().equals(major))
                studentsformajor.add(student);
        }
        return studentsformajor;

    }

    public List<Student> getAllStudentsPaginated(int start, int size){
        List<Student> list=new ArrayList<Student>(Students.values());
        if(start+size> list.size())
            return new ArrayList<Student>();
        return list.subList(start,start+size);

    }




}
