package org.acme.resteasy.Service;
import org.acme.resteasy.DB.Database;
import org.acme.resteasy.Model.Class;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClassService {
    private Map<Long, Class> Classes = Database.Classes;

    public ClassService() {
        StudentService s = new StudentService();
        Classes.put(1l, new Class(1, "Class1", s.getAllStudents()));
    }

    public List<Class> getAllClasses() {
        return new ArrayList<Class>(Classes.values());
    }

    public Class getClass(Long id) {
        return Classes.get(id);
    }


    public Class addClass(Class Class) {
        Class.setId(Classes.size() + 1);
        Classes.put((long) Class.getId(), Class);
        return Class;
    }

    public Class updateClass(Class Class) {
        if (Class.getId() <= 0)
            return null;
        else {
            Classes.put((long) Class.getId(), Class);
            return Class;
        }
    }

    public void removeClass(Long Class) {
        Classes.remove(Class);
    }


    public List<Class> getAllClassesName(String name) {
        List<Class> classesforname = new ArrayList<>();
        for (Class counterclass: Classes.values()){
            if (counterclass.getName().equals(name))
            classesforname.add(counterclass);
        }
        return classesforname;
    }


    public List<Class> getAllClassesPaginated(int start, int size){
        List<Class> list = new ArrayList<>(Classes.values());
        if(start+size> list.size())
            return new ArrayList<Class>();
        return list.subList(start,start+size);

    }
}